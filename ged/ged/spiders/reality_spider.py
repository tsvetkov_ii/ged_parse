import scrapy
import urllib2


# Spider class
class RealitySpider(scrapy.Spider):
    # spider name
    name = 'reality'

    # urls for scraping
    start_urls = [
        r'http://www.irn.ru/realty/',
    ]

    # reality spider method
    # attr response - response from url
    def parse(self, response):
        # loop for all reality
        for r in response.css('a.pb'):
            href = r.css('::attr(href)').extract_first()

            # get actual url

            try:
                resp = urllib2.urlopen(href)
                url = resp.geturl()
            except ValueError, urllib2.URLError:
                url = 'invalid url'

            yield {
                'title': r.css('span.b-photoblock_item_header::text').extract_first(),      # title
                'text': r.css('span.b-photoblock_item_content::text').extract_first(),      # text
                'image': r.css('img::attr(src)').extract_first(),                           # img
                'href': url,                                                                # href
            }
